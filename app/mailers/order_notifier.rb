class OrderNotifier < ApplicationMailer
  default from: 'Anton\'s Depot <depot@dark.mailgun.org>'

  def received(order)
    @order = order
    mail to: order.email, subject: 'Подтверждение заказа в Pragmatic Store'
  end

  def shipped(order)
    @order = order
    mail to: order.email, subject: 'Заказ из Pragmatic Store отправлен'
  end

end
