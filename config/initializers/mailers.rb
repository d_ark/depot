
unless Rails.env.test?
  Depot::Application.configure do
    config.action_mailer.delivery_method = :smtp
    config.action_mailer.smtp_settings = {
      address: "smtp.mailgun.org",
      port: 587,
      domain: "dark.mailgun.org",
      authentication: "plain",
      user_name: "postmaster@dark.mailgun.org",
      password: "46g3xpbfb4k2",
      enable_starttls_auto: true
    }
  end
end